/*
 * Write a program to calculate percentage marks of students using local classes
 */

package com.ftd.javatraining.day5;

public class Student {

	public int percentage() {
		
		class Marks{
			public int physics;
			public int maths;
			public int chem;
			public Marks(int phy,int math,int chem){
				this.physics=phy;
				this.maths=math;
				this.chem=chem;
			}
		}
		
		Marks m = new Marks(70,80,90);
	    return ((m.physics+m.maths+m.chem)/3);
			
	}
		
		
	
	public static void main(String[] args) {
		Student s = new Student();
		System.out.println(s.percentage());
		
	}

}
