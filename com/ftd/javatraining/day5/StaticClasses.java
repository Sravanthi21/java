/*
 * Write a program to calculate percentage marks of students using local classes
 */

package com.ftd.javatraining.day5;

public class StaticClasses {

	public static class Inner{
		public void display() {
			System.out.println("Printed");
		}
	}
	public static void main(String[] args) {
		StaticClasses.Inner obj = new StaticClasses.Inner();
		obj.display();

	}

}
