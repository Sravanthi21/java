/*
 * Write a program to access nested class members (variables & methods) in outer class methods
 */

package com.ftd.javatraining.day5;

public class InnerClassExample {

	public static void main(String[] args) {
		Animal a = new Animal();
		Animal.Dog dog = a.new Dog();
		Animal.Cat cat = a.new Cat();
		
		dog.action();
		cat.action();

	}

}
