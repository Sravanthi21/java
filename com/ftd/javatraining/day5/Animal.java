package com.ftd.javatraining.day5;

public class Animal {
	class Dog{
		void action() {
			System.out.println("Barks");
		}
	}
	
	class Cat{
		void action() {
			System.out.println("Meows");
		}
	}
	
}
