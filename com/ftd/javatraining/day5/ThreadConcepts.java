/*
 * Write a program to illustrate Thread and Thread pool concepts.
 */

package com.ftd.javatraining.day5;

public class ThreadConcepts extends Thread {
	
	public String s;
	ThreadConcepts(String s){
		this.s=s;
	}
	public void run() {
		try {
		int i=0;
		while(i<10) {
		System.out.println(s);
		sleep(1000);
		i++;
		}
		}
		catch(InterruptedException e) {
			System.out.println(e);
		}
	}
	public static void main(String[] args) {
		ThreadConcepts t1 = new ThreadConcepts("Hello");
		ThreadConcepts t2 = new ThreadConcepts("World");
		t1.start();
		t2.start();
		

	}

}

