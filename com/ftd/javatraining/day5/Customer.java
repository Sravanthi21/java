/*
 * 
 * Write a program to print below customer details using inner classes
	Customer Name
	Customer Age
	Customer email
	Customer phone
	Customer gender
	Customer Addresses (* multiple addresses)
 */

package com.ftd.javatraining.day5;

import java.util.Arrays;

public class Customer {

	public String name;
	public int age;
	public String email;
	public String phone;
	public String gender;
	public String[] addresses;
	
	Customer(String name,int age,String email,String phone,String gender,String[] addresses){
		this.name = name;
		this.age = age;
		this.email = email;
		this.phone = phone;
		this.gender = gender;
		this.addresses = addresses; 
	}
	public class Display{
		
		public void display() {
			System.out.println(name+" "+age+" "+email+" "+phone+" "+gender+" "+Arrays.toString(addresses));
		}
	}
	public static void main(String[] args) {
		String addresses[] = {"pdp","hyd"};
		Customer c = new Customer("Sravanthi",22,"srvanthi@gmaial.com","9849292192","female",addresses);
		
		Customer.Display dis = c.new Display();
		dis.display();
		
		
		

	}

}
