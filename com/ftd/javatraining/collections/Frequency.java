/*
 * 2.Take a file having 100(Or More) words do these operations.
     Find the duplicate words with their frequencies.
	 Find the word having Maximum Frequency.
	 Sort the words with respect to their frequency

 */

package com.ftd.javatraining.collections;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import static java.util.stream.Collectors.*;

public class Frequency {

	public static void main(String[] args) throws Exception {

		FileReader f = new FileReader("D:\\sravanthi\\freq.txt");
		BufferedReader br = new BufferedReader(f);
		String line;
		String []words;
		Map<String,Integer> frequency = new HashMap<String ,Integer>();
		while((line=br.readLine())!=null) {
			words = line.split(" ");
			for(String word:words) {
				if(frequency.get(word)==null) {
					frequency.put(word, 1);
				}
				else {
					Integer oldValue = frequency.get(word);
					frequency.put(word,oldValue+1);
				}
			}
		}
		System.out.println(frequency);
		System.out.println();
		System.out.println("Duplicate Words");
		Integer val=0;
		Integer current;
		List <String> maxFreq = new ArrayList<String>();
		for(Map.Entry<String, Integer> entry:frequency.entrySet()) {
			if(entry.getValue() >1) {
				System.out.println(entry.getKey());
			}
			current=entry.getValue();
			if(current>val) {
				val=current;
				maxFreq.clear();
				maxFreq.add(entry.getKey());
			}
			else if (current==val) {
				maxFreq.add(entry.getKey());
			}
		}
		System.out.println();
		System.out.println("Max frequency");
		System.out.println(maxFreq);
		System.out.println();
		System.out.println("Sorted Frequencies");
		List<Map.Entry<String, Integer> > list = 
	               new ArrayList<Map.Entry<String, Integer> >(frequency.entrySet()); 
		Collections.sort(list, new Comparator<Map.Entry<String, Integer> >() { 
            public int compare(Map.Entry<String, Integer> o1,  
                               Map.Entry<String, Integer> o2) 
            { 
                return (o2.getValue()).compareTo(o1.getValue()); 
            } 
        }); 
		System.out.println(list);
          

		f.close();

	}

}
