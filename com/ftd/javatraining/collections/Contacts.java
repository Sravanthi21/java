package com.ftd.javatraining.collections;

import java.util.ArrayList;
import java.util.List;

public class Contacts {

	public String firstName;
	public String lastName;
	//public String [] phone = new String [2];
	public List<String> phone;
	public String company;
	
	Contacts(String firstName,String lastName,ArrayList<String> phone, String company) {
		try {
		if(phone.size()<=2) {
			this.phone = phone;
			}
		else {
			throw new Exception();
		}
		this.firstName=firstName;
		this.lastName=lastName;
		
		this.company=company;
		System.out.println("Object "+firstName+" "+lastName+" Created");
		}
		catch(Exception e) {
			System.out.println("Object not created "+e);
		}
		
	}
	
	 public String toString() {
	        return ("FirstName:"+this.firstName+
	                    " lastName: "+ this.lastName +
	                    " contacts "+ this.phone +
	                    " Company: " + this.company);
	   }
	
}
