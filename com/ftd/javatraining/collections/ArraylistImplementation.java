/*
 * 4.Create Your own Implementation of ArrayList with following operation
	add(Integer obj).(Add an element to the list)
	get(integer index).(Get an element from the index)
	remove(Integer obj).(Remove the specified object)
	contains(Integer obj).(Returns whether element exist or not in the list)
	Sort(List):It should sort the list in ascending order.

 */

package com.ftd.javatraining.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ArraylistImplementation {

	public static void main(String[] args) {
		
		List <Integer> arr=new ArrayList<Integer>();
		for(Integer i=10;i>0;i--) {
		arr.add(i);
		}
		System.out.println(arr);
		System.out.println(arr.get(3));
		arr.remove(3);
		System.out.println(arr);
		System.out.println(arr.contains(6));
		Collections.sort(arr);
		System.out.println(arr);
	}

}
