/*
 * 1.Write a Program to achieve following operations using one or More Implementation of Collection Framework.
	User can be able to Create a Phone Book.
	User  can be able to Add Contacts to Phone Book
	A contact can have Three attributes{Name, phone Number: work/Home, Company Name}
	A contact can have max of two phone numbers i.e: Work and Home.
	No Two Contacts will have same Name(First and Last).
	Contacts can be searched via Name.
	Contacts Can be searched via Phone number.
	Contacts can be deleted via phone Number or Name.
	Contacts can be sorted via names.
	Contacts Can be grouped Via Company Names.

 */

package com.ftd.javatraining.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CollectionExample {
	
	List<Contacts> phoneBook;
	
	public void createPhoneBook(){
		phoneBook= new ArrayList<Contacts>();
		System.out.println("Phone book created");
	}
	
	public void addContactToPhoneBook(Contacts contact) {
		if(contact.firstName==null) {
			System.out.println("Object not valid or created");
			return;
		}
		for(Contacts c:phoneBook) {
			
			
			if(c.firstName.equalsIgnoreCase(contact.firstName) && c.lastName.equalsIgnoreCase(contact.lastName)) {
				System.out.println("Contact already exists.Couldn't add to the phone book.");
				return;
			}
			
		}
		System.out.println("added "+contact.firstName+" "+contact.lastName);
		phoneBook.add(contact);
	}
	
	public Contacts searchViaName(String firstName,String lastName) {
		
		for(Contacts c:phoneBook) {
			if(c.firstName.equalsIgnoreCase(firstName) && c.lastName.equalsIgnoreCase(lastName)) {
				return c;
			}
		}
		System.out.println("Contact not found");
		return null;
	}
	
	public Contacts searchViaPhone(String number) {
		for(Contacts c:phoneBook) {
			if(c.phone.contains(number)) {
				return c;
			}
		}
		System.out.println("Contact not found");
		return null;
	}
	
	public void deleteViaName(String firstName,String lastName) {
		for(Contacts c:phoneBook) {
			if(c.firstName.equalsIgnoreCase(firstName) && c.lastName.equalsIgnoreCase(lastName)) {
				phoneBook.remove(c);
				return;
			}
		}
		System.out.println("Contact not found.");
	}
	
	public void deleteViaNumber(String number) {
		for(Contacts c:phoneBook) {
			if(c.phone.contains(number)) {
				c.phone.remove(number);
				return;
			}
		}
		System.out.println("Contact not found.");
	}
	
	public void sortViaNames() {
		Collections.sort(phoneBook, new Comparator<Contacts>() {
		    public int compare(Contacts c1, Contacts c2) {
		    	int comp2;
		        int comp1 = c1.firstName.compareTo(c2.firstName);
		        if(comp1 == 0) {
		        	comp2=c1.lastName.compareTo(c2.lastName);
		        	return comp2;
		        }
		        return comp1;
		    }
		});
	}
	
	public List<Contacts> groupViaCompany(String comp){
		List<Contacts> compContacts = new ArrayList<Contacts>();
		for(Contacts c:phoneBook) {
			if(c.company.equals(comp)) {
				compContacts.add(c);
			}
		}
		return compContacts;
	}
	public static void main(String[] args) {
		CollectionExample obj = new CollectionExample();
		
		ArrayList<String> l1 = new ArrayList<String>();
		l1.add("9849292192");
		l1.add("7893079518");
		l1.add("9989499345");
		
		ArrayList<String> l2 =new ArrayList<String>();
		l2.add("9090909090");
		
		ArrayList<String> l3 =new ArrayList<String>();
		l3.add("9849840861");
		l3.add("9849838347");
		
		ArrayList<String> l4 =new ArrayList<String>();
		l4.add("7893079518");
		l4.add("9849292192");
		
		ArrayList<String> l5 =new ArrayList<String>();
		l5.add("9876543211");
		
		Contacts c1 = new Contacts("Sravanthi","V",l1,"FTD");
		Contacts c2 = new Contacts("Someone","Something",l2,"SomeCompany");
		Contacts c3 = new Contacts("Sravanthi","V",l3,"FTD");
		Contacts c4 = new Contacts("Sravanthi","V",l3,"FTD");
		Contacts c5 = new Contacts("Sowmya","B",l4,"FTD");
		Contacts c6 = new Contacts("Paddu","K",l5,"FTD");
		
		obj.createPhoneBook();
		System.out.println();
		
		
		obj.addContactToPhoneBook(c1);
		obj.addContactToPhoneBook(c2);
		obj.addContactToPhoneBook(c3);
		obj.addContactToPhoneBook(c4);
		obj.addContactToPhoneBook(c5);
		obj.addContactToPhoneBook(c6);
		
		System.out.println(obj.phoneBook);
		
		
		System.out.println("Search via name");
		System.out.println(obj.searchViaName("Sravanthi", "V"));
		System.out.println(obj.searchViaName("sona", "v"));
		System.out.println();
		
		System.out.println("Search via number");
		System.out.println(obj.searchViaPhone("9849292192"));
		System.out.println(obj.searchViaPhone("9098989898"));
		System.out.println();
		
		System.out.println("Deleted by name");
		System.out.println("First");
		obj.deleteViaName("Paddu", "K");
		System.out.println(obj.phoneBook);
		System.out.println("Second");
		obj.deleteViaName("Paddu", "K");
		System.out.println(obj.phoneBook);
		System.out.println();
		
		System.out.println("Deleted number 9989267276");
		obj.deleteViaNumber("9989267276");
		System.out.println(obj.phoneBook);
		System.out.println();
		
		obj.sortViaNames();
		System.out.println("After sort");
		System.out.println(obj.phoneBook);
		System.out.println();
		
		System.out.println("Grouped via FTD");		
		System.out.println(obj.groupViaCompany("FTD"));
		System.out.println();
	}

	

}
