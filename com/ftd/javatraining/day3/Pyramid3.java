/*
 c.     1
       2 2
	  3 3 3 
	 4 4 4 4
	5 5 5 5 5 	
 
 */

package com.ftd.javatraining.day3;

public class Pyramid3 {

	public static void main(String[] args) {
		for(int i=1;i<=5;i++)
		{
			for(int k=0;k<(6-i);k++) {              //  n-i+1
			System.out.print(" ");
			}
			for(int j=0;j<i;j++) {
				System.out.print(i+" ");
			}
			System.out.println();
		}

	}

}
