package com.ftd.javatraining.day3;
import com.ftd.javatraining.day3.MyException;
public class CustomException {

	
 void validPhNumber(int noOfDigits) throws MyException{
		if(noOfDigits<10) {
			throw (new MyException("Not a valid ph number"));
		}
		
	}
		
	public static void main(String[] args) {
		CustomException ce = new CustomException();
		try {
			ce.validPhNumber(5);
			System.out.println("valid number");
		}
		catch (Exception e){
			System.out.println("Exception caught"+e.getMessage());
		}
		System.out.println("Completed");
	}

}
