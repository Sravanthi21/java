/*
1.Evaluate below (a=5 & b=-8)
		a&b		a|b		a^b		b>>1	b>>>2
		
*/



package com.ftd.javatraining.day3;

public class Evaluation {

	public static void main(String[] args) {
		int a=5;
		int b=-8;
		
		System.out.println("a&b = "+(a&b));
		System.out.println("a|b = "+(a|b));
		System.out.println("a^b = "+(a^b));
		System.out.println("b>>1 = "+(b>>1));
		System.out.println("b>>>2 = "+(b>>>2));

	}

}
