/*
 * 2.write a method that takes month as input argument and return number of days of the month using all conditional branching statements (If Else ladder & Switch case).
 * 
 */

package com.ftd.javatraining.day3;
import java.util.List;
import java.util.Arrays;
import java.util.Scanner; 

public class NoOfDays {

	public int noOfDaysIf(String month) {
		 String [] arr31 = {"January","March","May","July","August","October","December"};
		 String [] arr30 = {"April","June","September","November"};
		 List<String> list31 = Arrays.asList(arr31);
		 List<String> list30 = Arrays.asList(arr30);

		if( list31.contains(month)) {
			return 31;
		}
		else if (list30.contains(month)) {
			return 30;
		}
		else if(month.equals("February")) {
			return 28;
		}
		return 0;
		
	}
	public void noOfDaysSwitch(String month) {
		switch(month) {
		case "January":
		case "March":
		case "May":
		case "July": 
		case "August":
		case "October": 
		case "December":
			System.out.println("31 days in this month");
			break;
		case "April":
		case "June":
		case "September":
		case "November":
			System.out.println("30 days in this month");
		case "February":
			System.out.println("28 days in this month");
		}
	}
	public static void main(String[] args) {
		Scanner s  = new Scanner(System.in);
		System.out.println("Enter a month");
		String month = s.nextLine();
		NoOfDays n = new NoOfDays();
		System.out.println(month+" has "+n.noOfDaysIf(month)+" days.");
		n.noOfDaysSwitch(month);
	}

}
