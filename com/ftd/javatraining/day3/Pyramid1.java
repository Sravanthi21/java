/*
5.Write a program to print below forms using for loop
  a.1
	1 1
	1 1 1
	1 1 1 1
 
 
 */


package com.ftd.javatraining.day3;

public class Pyramid1 {

	public static void main(String[] args) {
		for( int i=1;i<=4;i++)
		{
			for( int j=0;j<i;j++)
			{
				System.out.print("1 ");
			}
			System.out.println();
		}

	}

}
