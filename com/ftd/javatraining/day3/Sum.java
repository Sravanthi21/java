/*
 * 3.Write a program to calculate the sum of all integers between 100 and 200 which are divisible by 7 using all types of loop statements.
 * 
 */
package com.ftd.javatraining.day3;
public class Sum {

	public static void main(String[] args) {
		int sum=0;
		for(int i=100;i<=200;i++) {
			if(i%7 ==0) {
				sum += i;
			}
				
		}
		System.out.println("Required Sum using for loop " +sum);
		
		sum = 0;
		int i = 100;
		while(i<=200) {
			if(i%7 == 0) {
				sum +=i;
			}
			i++;
		}
		System.out.println("Required sum using while loop "+sum);
		
		sum = 0;
		i = 100;
		do {
			if(i%7 == 0) {
				sum +=i;
			}
			i++;
		}while(i<=200);
		System.out.println("Required sum using do while loop "+sum);
	}
		
}
