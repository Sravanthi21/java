/*
 * 1) Two files contains list of phone numbers. Create a new file that contains only phone numbers from file1 but not in file2.

 */
package com.ftd.javatraining.day4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PhoneNumbers {

	public static void main(String[] args) throws IOException {
		String line=null;
		FileReader fr1 = new FileReader("D:\\sravanthi\\file1.txt");
		FileReader fr2 = new FileReader("D:\\sravanthi\\file2.txt");
		BufferedReader br1 = new BufferedReader(fr1);
		BufferedReader br2 = new BufferedReader(fr2);
		List<String> l1 = new ArrayList<String>();
		List<String> l2 = new ArrayList<String>();
		while((line = br1.readLine())!=null) {
			l1.add(line);	
		}
		line = null;
		while((line = br2.readLine())!=null) {
			l2.add(line);	
		}
		
		l1.removeAll(l2);
		BufferedWriter writer = new BufferedWriter(new FileWriter("D:\\sravanthi\\file3.txt"));
		for(String str:l1) {
		writer.write(str);
		writer.newLine(); 	
		}
		writer.close();
		System.out.println(l1);
	}

}
