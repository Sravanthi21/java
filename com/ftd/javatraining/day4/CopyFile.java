/*
 * 2) Copy the content of a file into a new file using buffered reader.
 */

package com.ftd.javatraining.day4;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class CopyFile {

	public static void main(String[] args) throws IOException {
		String line=null;
		FileReader f = new FileReader("D:\\sravanthi\\one.txt");
		FileWriter f2 = new FileWriter("D:\\sravanthi\\two.txt");
		BufferedReader br = new BufferedReader(f);
		BufferedWriter bw = new BufferedWriter(f2);
		while((line= br.readLine())!=null) {
			System.out.println(line);
		bw.write(line);
		bw.newLine();
		}
		bw.close();
		br.close();
	}

}
