/*
 * 3) write a program using at-least 10 string methods
 */

package com.ftd.javatraining.day4;

public class StringMethods {

	public static void main(String[] args) {
	
		String s1=new String("Hello World");
		
		System.out.println(s1.indexOf("r"));
		System.out.println(s1.substring(6));
		System.out.println(s1.toUpperCase());
		System.out.println(s1.charAt(7));
		System.out.println(s1.toLowerCase());
		System.out.println(s1.equalsIgnoreCase("hello world"));
		System.out.println(s1.toCharArray());
		System.out.println(s1.length());
		System.out.println(s1.substring(3, 7));
		System.out.println(s1.compareTo("hello world"));
				
	}

}
