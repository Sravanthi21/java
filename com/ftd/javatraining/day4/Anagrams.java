/*
 * 1)Write a program to check whether two given strings are anagrams are not
 */

package com.ftd.javatraining.day4;

import java.util.Arrays;
import java.util.Scanner;

public class Anagrams {

	private static Scanner s;

	public static void main(String[] args) {
		s = new Scanner(System.in);
		String s1=s.nextLine();
		String s2=s.nextLine();
		if(s1.length() != s2.length()){
			System.out.println("Not anagrams");
			return;
		}
		char[] arr1 = s1.toCharArray();
		char[] arr2 = s2.toCharArray();
		Arrays.sort(arr1);
		Arrays.sort(arr2);
		for(int i=0;i<s1.length();i++) {
			if(arr1[i] != arr2[i]) {
				System.out.println("Not anagrams");
				return;
			}
		}
		System.out.println("Anagrams");
		
		return;

	}

}
