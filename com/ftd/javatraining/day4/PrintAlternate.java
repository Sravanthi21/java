/*
 * 4)Write a program to read input from console and print every alternate character.
 */

package com.ftd.javatraining.day4;

import java.util.Scanner;

public class PrintAlternate {

	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		
		String input = s.nextLine();
		char inputArr[] = input.toCharArray();
		for (int i=0;i<inputArr.length;i=i+2)
		{
			System.out.print(inputArr[i]);
		}

	}

}
