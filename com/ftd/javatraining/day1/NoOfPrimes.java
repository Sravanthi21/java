/*
 * 4.Write a program to count the number of prime numbers that are less than 100 using all types of loop statements.
 */

package com.ftd.javatraining.day1;

public class NoOfPrimes {

	public static void main(String[] args) {
		
		int noOfPrimes = 0;
		for(int i=1;i<100;i++) {
			int counter = 0;
			for(int n=1;n<=(i/2);n++) {
				if(i%n == 0)
				{
					counter+=1;
				}
			}
			if(counter == 1) {
				System.out.println(i);
				noOfPrimes +=1;
			}

		}
		System.out.println("For Loop--- No of primes less than 100 are "+noOfPrimes);
		
		noOfPrimes =0;
		int i=1;
		
		while(i<100) {
			int counter = 0;
			int n=1;
			while(n<=(i/2)) {
				if(i%n == 0) {
					counter+=1;
				}	
				n++;

			}
			if(counter == 1) {
				System.out.println(i);
				noOfPrimes +=1;
			}
					
			i++;
		}
		System.out.println("While Loop --- No of primes less than 100 are "+noOfPrimes);
		
		noOfPrimes = 0;
		i=2;
		
		do {
			int n=1;
			int counter = 0;
			do {
				if(i%n == 0) {
					counter+=1;
				}
				n++;
			}while(n<=(i/2));
			if(counter ==1) {
				System.out.println(i);
				noOfPrimes+=1;
			}
			i++;
		}while(i<100);
		System.out.println("Do While Loop --- No of primes less than 100 are "+noOfPrimes);
			
		
	}

}
