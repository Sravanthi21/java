/*

2. Create class "Author" with following specifications
	a. It should contain properties name(String),email(String),gender(String), numberOfBooksPublished(integer), 
	    bookNames (array of strings)
	b. The class should have a constructor should parameters name, email ,gender and numberOfBooksPublished
	c. The constructor should take the parameters and initialize the object properties accordingly. Based on the
	    numberOfBooksPublished parameter, it should initialize the bookNames array with length of numberOfBooksPublished.
	d. A method getBookNames() should be provided in the class which returns bookNames array.

*/
package com.ftd.javatraining.day1;

class Author{
	
	public String name;
	public String email;
	public String gender;
	public int numberOfBooksPublished;
	public String bookNames[];
	
	Author(String name, String email,String gender,int numberOfBooksPublished){
		this.name = name;
		this.email = email;
		this.gender = gender;
		this.numberOfBooksPublished = numberOfBooksPublished;
		bookNames = new String[this.numberOfBooksPublished];
	}
	
	public String[] getBookNames(){
		return bookNames;
	}
}