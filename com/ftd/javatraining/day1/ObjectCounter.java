/**

1. Write a class with name "ObjectCounter" which with following specifications. 
   a. It should contain a variable "noOfObjects" which maintains number of objects created. 
   b. For every object created, the variable should be incremented by 1. 
   c. A method getNoOfObjects() should be provided in the class which returns number of objects created.

Write a main method in the class "ObjectCounter" which crates three objects of the class. 
After creating the three objects, call the method getNoOfObjects() and print the returned value.

*/
package com.ftd.javatraining.day1;

import java.util.concurrent.TimeUnit;

class ObjectCounter{
	public static int noOfObjects;
	
	static {
		System.out.println("static block");
		noOfObjects = 0;
	}
	
	ObjectCounter(){
		noOfObjects += 1;
	}
	
	public static int getNoOfObjects(){
		return noOfObjects;
	}
	
	public static void main(String args[]) throws InterruptedException {
		TimeUnit.SECONDS.sleep(2);
		ObjectCounter obj1 = new ObjectCounter();
		ObjectCounter obj2 = new ObjectCounter();
		ObjectCounter obj3 = new ObjectCounter();
		System.out.println(ObjectCounter.getNoOfObjects());
		
		
	}
}